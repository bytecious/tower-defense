import { Scene } from 'phaser';
import bgHome from '../assets/img/bg-home.png';
import logoHome from '../assets/img/home-logo.png';
import campaignButton from '../assets/img/campaign-button.png';

export default class MainScene extends Scene {
    constructor () {
        super({ key: 'main' });
    }

    preload () {
        this.load.image('bgHome', bgHome);
        this.load.image('logoHome', logoHome);
        this.load.image('btnCampaign', campaignButton);
    }

    create () {
        this.scene.start('menu');
    }
}
