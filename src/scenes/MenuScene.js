import { Scene } from 'phaser';

export default class MenuScene extends Scene {
    constructor () {
        super({ key: 'menu' })
    }

    create () {
        let textString = "JUGAR";
        let text = this.add.text(550, 550, textString, {
            font: "2rem Arial",
            fontWeight: 'bold',
            fill: "#FFFFFF"
        });

        text.setInteractive();
        text.on('pointerdown', () => { 
            this.scene.switch('home');
        });

        this.add.image(350, 0, 'logoHome').setOrigin(0, 0).setScale(0.8);
    }
}
