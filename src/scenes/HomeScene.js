import { Scene } from 'phaser';

export default class HomeScene extends Scene {
    constructor () {
        super({ key: 'home' })
    }

    create () {
        this.add.image(0, 0, 'bgHome').setOrigin(0, 0);
        this.add.image(200, 150, 'btnCampaign').setOrigin(0, 0);
    }
}
