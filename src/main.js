import Phaser from 'phaser';
import MainScene from './scenes/MainScene';
import HomeScene from './scenes/HomeScene';
import MenuScene from './scenes/MenuScene';

const config = {
    type: Phaser.AUTO,
    parent: 'app',
    width: 1200,
    height: 600,
    scene: [MainScene, HomeScene, MenuScene]
};

const game = new Phaser.Game(config);
window.game = game;
